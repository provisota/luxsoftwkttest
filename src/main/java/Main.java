import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Alterovych Ilya
 */
public class Main implements Observer {
    ArrayBlockingQueue<String> readFilesQueue;
    ArrayBlockingQueue<WKTFile> writeFilesQueue;
    private static Logger LOG = Logger.getLogger(Main.class.getName());

    private final String inputDir;
    private final String outputDir;
    private final Watcher watcher;

    private static Thread scannerThread;
    private static Thread readThread;
    private static Thread writeThread;

    public Main(String inputDir, String outputDir) {
        this.inputDir = inputDir;
        this.outputDir = outputDir;
        readFilesQueue = new ArrayBlockingQueue<String>(100);
        writeFilesQueue = new ArrayBlockingQueue<WKTFile>(100);
        watcher = new Watcher(inputDir);
        watcher.registerObserver(this);
    }

    private void init() {
        File input = new File(inputDir);
        File[] newFiles = input.listFiles();
        if (newFiles == null)
            return;
        for (File newFile : newFiles) {
            try {
                readFilesQueue.put(newFile.getAbsolutePath());
            } catch (InterruptedException e) {
                LOG.error(e.getMessage(), e);
            }
        }

//        System.out.println("Начальные файлы во входной директории:");
//        for (String file : readFilesQueue){
//            System.out.println(file);
//        }

        scannerThread = new Thread(watcher);
        readThread = new Thread(new Reader(this, outputDir));
        writeThread = new Thread(new Writer(this));

        scannerThread.start();
        readThread.start();
        writeThread.start();
    }

    public static void main(String[] args) {
        String inDir;
        String outDir;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter input Directory please: ");
            inDir = reader.readLine();
            System.out.println("Enter output Directory please: ");
            outDir = reader.readLine();
            Main main = new Main(inDir, outDir);
            main.init();
            do {
                System.out.println("Enter <exit> to exit: ");
            }
            while (!"exit".equalsIgnoreCase(reader.readLine()));
            LOG.info("closing application");
            stopApp();
            reader.close();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    private static void stopApp() {
        scannerThread.interrupt();
        readThread.interrupt();
        writeThread.interrupt();
    }

    @Override
    public void update(String filename) {
        try {
            readFilesQueue.put(filename);
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
//        System.out.println("новый файл добавился: " + filename);
    }
}
