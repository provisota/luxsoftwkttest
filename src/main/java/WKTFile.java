import com.vividsolutions.jts.geom.Geometry;

/**
 * @author Alterovych Ilya
 */
public class WKTFile {
    private final String path;
    private final Geometry polygon;

    public WKTFile(String path, Geometry polygon) {
        this.path = path;
        this.polygon = polygon;
    }

    public String getPath() {
        return path;
    }

    public Geometry getPolygon() {
        return polygon;
    }
}
