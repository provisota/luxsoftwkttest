import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

/**
 * @author Alterovych Ilya
 */
public class Watcher implements Runnable, Observable {
    private CopyOnWriteArrayList<Observer> observers;
    private Path dir;
    private String filename;
    private static Logger LOG = Logger.getLogger(Watcher.class.getName());

    public Watcher(String dir) {
        this.dir = Paths.get(dir);
        observers = new CopyOnWriteArrayList<Observer>();
    }

    @Override
    public void run() {

        while (!Thread.currentThread().isInterrupted()) {
            WatchService watcher;
            WatchKey key;
            try {
                watcher = FileSystems.getDefault().newWatchService();
                key = dir.register(watcher, ENTRY_CREATE);
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
                return;
            }

            while (true){
                try {
                    key = watcher.take();
                } catch (InterruptedException e) {
                    return;
                }

                for (WatchEvent<?> event: key.pollEvents()){
                    WatchEvent.Kind<?> kind = event.kind();
                    if (kind == OVERFLOW) {
                        continue;
                    }
                    WatchEvent<Path> ev = cast(event);
                    LOG.info(String.format("new file named %s added to input directory"
                            , ev.context()));
                    filename = dir.resolve(ev.context()).toString();
                    notifyObservers();
                }

                boolean valid = key.reset();
                if (!valid) {
                    break;
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<Path> cast(WatchEvent<?> event) {
        return (WatchEvent<Path>)event;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(filename);
        }
    }
}
