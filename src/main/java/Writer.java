import com.vividsolutions.jts.io.WKTWriter;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Alterovych Ilya
 */
public class Writer implements Runnable {
    private Main main;
    private static Logger LOG = Logger.getLogger(Writer.class.getName());

    public Writer(Main main) {
        this.main = main;
    }

    @Override
    public void run() {

        while (!Thread.currentThread().isInterrupted()){
            BufferedWriter fileWriter = null;
            try {
                WKTFile wktFile = main.writeFilesQueue.take();
                WKTWriter wktWriter = new WKTWriter();
                fileWriter = new BufferedWriter(new FileWriter(wktFile.getPath()));
                fileWriter.write(wktWriter.write(wktFile.getPolygon()));
                LOG.info(String.format("create and write new file %s, it contents %s"
                        , wktFile.getPath(), wktFile.getPolygon()));
                fileWriter.flush();
                fileWriter.close();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            } finally {
                if (fileWriter != null) {
                    try {
                        fileWriter.close();
                    } catch (IOException e) {
                        LOG.error(e.getMessage(), e);
                    }
                }
            }
        }
    }
}
