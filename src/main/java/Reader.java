import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Alterovych Ilya
 */
public class Reader implements Runnable {
    private String outputDir;
    private Main main;
    private String inputFilename;
    private CopyOnWriteArrayList<String> oldFilesList;
    private static Logger LOG = Logger.getLogger(Reader.class.getName());

    public Reader(Main main, String outputDir) {
        this.outputDir = outputDir;
        this.main = main;
        oldFilesList = new CopyOnWriteArrayList<String>();
    }

    @Override
    public void run() {

        while (!Thread.currentThread().isInterrupted()) {
            try {
                inputFilename = main.readFilesQueue.take();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
            if (oldFilesList.size() == 0) {
                oldFilesList.add(inputFilename);
                continue;
            }

            Geometry newPolygon = readPolygon(inputFilename);
            if (newPolygon == null)
                continue;
            findIntersections(newPolygon);
            oldFilesList.add(inputFilename);
        }
    }

    private void findIntersections(Geometry newPolygon) {
        for (String oldFileName : oldFilesList) {
            Geometry oldPolygon = readPolygon(oldFileName);
            if (oldPolygon == null){
                oldFilesList.remove(oldFileName);
                continue;
            }
            Geometry intersectedPolygon = newPolygon.intersection(oldPolygon);
            String newFilename = getNewFileName(inputFilename, oldFileName);
            try {
                main.writeFilesQueue.put(new WKTFile(newFilename, intersectedPolygon));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private String getNewFileName(String inputFilename, String oldFileName) {
        outputDir = outputDir.endsWith("/") || outputDir.endsWith("\\") ?
                outputDir : outputDir + "\\";
        return outputDir + new File(inputFilename).getName().replace(".wkt", "_") +
                new File(oldFileName).getName();
    }

    private Geometry readPolygon(String inputFilename) {
        WKTReader wktReader = new WKTReader();
        Geometry polygon = null;
        BufferedReader reader = null;
        try {
            if (!inputFilename.endsWith(".wkt")) {
                throw new ParseException(inputFilename + " is not WKT file");
            }
            reader = new BufferedReader(new FileReader(inputFilename));
            polygon = wktReader.read(reader.readLine());
        } catch (ParseException e) {
            LOG.error(e.getMessage(), e);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return polygon;
    }
}
