/**
 * @author Alterovych Ilya
 */
public interface Observer {
    void update (String filename);
}
